api.model = api.model || {};

/*En meta se necesita el nombre
**del room para saber donde esta*/

api.model.device = class {
  constructor(typeId, name, meta){
    if(typeId){
      this.typeId = typeId;
    }else{
      delete(this.typeId);
    }
    this.name = name;
    this.meta = meta;
  }
}
