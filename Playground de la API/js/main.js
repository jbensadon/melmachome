var resultTextArea, inputarea;
// Room buttons
var room;
var roomCreate, roomUpdate, roomDelete, roomGet, roomGetAll, showdevicesButton;
var device1, device2;

//devices
var device;
var devCreate, devUpdate, devDelete, devGet, devGetAll, devShowRoom;

//routines
var routine;
var routineRooms, routineDevices;
var routCreate, routUpdate, routDelete, routGet,
    routGetAll, routGetRooms, routGetDevices, routGetActions;


window.addEventListener('load', function() {
  //general
  resultTextArea = document.querySelector('#result');
  inputarea = document.querySelector("#roomchange");
  //room
  roomCreate = document.querySelector('#roomcreate');
  roomUpdate = document.querySelector('#roomupdate');
  roomDelete = document.querySelector('#roomdelete');
  roomGet = document.querySelector('#roomget');
  roomGetAll = document.querySelector('#roomgetAll');
  showdevicesButton = document.querySelector("#showDevices");
  //device
  devCreate = document.querySelector('#devCreate');
  devUpdate = document.querySelector('#devUpdate');
  devDelete = document.querySelector('#devDelete');
  devGet = document.querySelector('#devGet');
  devGetAll = document.querySelector('#devGetAll');
  devShowRoom = document.querySelector('#showRoom');
  //routines
  routCreate = document.querySelector("#routCreate");
  routUpdate = document.querySelector("#routUpdate");
  routDelete = document.querySelector("#routDelete");
  routGet = document.querySelector("#routGet");
  routGetAll = document.querySelector("#routGetAll");
  routGetRooms = document.querySelector("#routGetRooms");
  routGetDevices = document.querySelector("#routGetDevices");
  routGetActions = document.querySelector("#routGetActions");

  //room's functions
  roomCreate.addEventListener('click', function() {
    var index = Math.floor(Math.random() * (999 - 1) + 1)
    room = new api.model.room(null, 'kitchen ' + index, '{ "size":"9m2" , "devices":["device1", "device2"] }');

    api.room.add(room)
    .then((data) => {
      room.id = data.room.id;
      resultTextArea.innerHTML = JSON.stringify(data, null, 2);
    })
    .catch((error) => {
      resultTextArea.innerHTML = 'Request failed: ' + error;
    });
  }, false);

  roomUpdate.addEventListener('click', function() {
    room.meta = '{ ' + inputarea.value + ' }';

    api.room.modify(room)
    .then((data) => {
      resultTextArea.innerHTML = JSON.stringify(data, null, 2);
    })
    .catch((error) => {
      resultTextArea.innerHTML = 'Request failed: ' + error;
    });
  }, false);

  roomDelete.addEventListener('click', function() {
    api.room.delete(room.id)
    .then((data) => {
      resultTextArea.innerHTML = JSON.stringify(data, null, 2);
    })
    .catch((error) => {
      resultTextArea.innerHTML = 'Request failed: ' + error;
    });
  }, false);

  roomGet.addEventListener('click', function() {
    api.room.get(room.id)
    .then((data) => {
      resultTextArea.innerHTML = JSON.stringify(data, null, 2);
    })
    .catch((error) => {
      resultTextArea.innerHTML = 'Request failed: ' + error;
    });
  }, false);

  roomGetAll.addEventListener('click', function() {
    api.room.getAll()
    .then((data) => {
      resultTextArea.innerHTML = JSON.stringify(data, null, 2);
    })
    .catch((error) => {
      resultTextArea.innerHTML = 'Request failed: ' + error;
    });
  }, false);

  /*Primero hay que parsear el string de meta para convertirlo en
  **un objeto, después de eso hay que agarrar la propiedad que queremos*/
  showdevicesButton.addEventListener('click', function(){
    api.room.get(room.id)
    .then((data) => {
      var deviceslist = JSON.parse(data.room.meta);
      deviceslist = deviceslist.devices;
      resultTextArea.innerHTML = JSON.stringify(deviceslist, null, 2);
    })
    .catch((error) =>{
      resultTextArea.innerHTML = 'Request failed: ' + error;
    });
  }, false);



//------------------------------------------------------------device's functions

devCreate.addEventListener('click', function() {
  var index = Math.floor(Math.random() * (999 - 1) + 1);
  device = new api.model.device("go46xmbqeomjrsjr", 'Lampara ' + index,
                      '{ "tipo": "de techo", "habitacion":"Cocina" }');

  api.device.add(device)
  .then((data) => {
    device.id = data.device.id;
    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
  })
  .catch((error) => {
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

devUpdate.addEventListener('click', function() {
  device.meta = '{ ' + inputarea.value + ' }';

  api.device.modify(device)
  .then((data) => {
    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
  })
  .catch((error) => {
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

devDelete.addEventListener('click', function() {
  api.device.delete(device.id)
  .then((data) => {
    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
  })
  .catch((error) => {
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

devGet.addEventListener('click', function() {
  api.device.get(device.id)
  .then((data) => {
    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
  })
  .catch((error) => {
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

devGetAll.addEventListener('click', function() {
  api.device.getAll()
  .then((data) => {
    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
  })
  .catch((error) => {
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

devShowRoom.addEventListener('click', function(){
  api.device.get(device.id)
  .then((data) => {
    var roomName = JSON.parse(data.device.meta);
    roomName = roomName.habitacion;
    resultTextArea.innerHTML = JSON.stringify(roomName, null, 2);
  })
  .catch((error) =>{
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

//-----------------------------------------------------------routine's functions

routCreate.addEventListener('click', function() {
  var index = Math.floor(Math.random() * (999 - 1) + 1);
  /*Los `` ya están parseando el string por lo que no hace falta el JSON.parse*/
  routine = new api.model.routine("good night", `[ { \"deviceId\": "${device.id}" , \"actionName\": \"turnOff\", \"params\": [], \"meta\": \"{}\" } ]`,'{"habitaciones":["Cocina", "Comedor"]}');

  api.routine.add(routine)
  .then((data) => {
    routine.id = data.routine.id;
    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
  })
  .catch((error) => {
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

//solo cambia el nombre
routUpdate.addEventListener('click', function() {
  routine.name = inputarea.value;

  api.routine.modify(routine)
  .then((data) => {
    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
  })
  .catch((error) => {
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

routDelete.addEventListener('click', function() {
  api.routine.delete(routine.id)
  .then((data) => {
    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
  })
  .catch((error) => {
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

routGet.addEventListener('click', function() {
  api.routine.get(routine.id)
  .then((data) => {
    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
  })
  .catch((error) => {
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

routGetAll.addEventListener('click', function() {
  api.routine.getAll()
  .then((data) => {
    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
  })
  .catch((error) => {
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);


routGetRooms.addEventListener('click', function(){
  api.routine.get(routine.id)
  .then((data) => {
    var roomsList = JSON.parse(data.routine.meta);
    roomsList = roomsList.habitaciones;
    resultTextArea.innerHTML = JSON.stringify(roomsList, null, 2);
  })
  .catch((error) =>{
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

routGetDevices.addEventListener('click', function(){

  api.routine.get(routine.id)
  .then((data) => {
    var devicesIds = [];
    var devicesList = data.routine.actions;
    for(var i = 0; i < devicesList.length; i++){
        devicesIds[i] = devicesList[i].deviceId;
    }
    resultTextArea.innerHTML = JSON.stringify(devicesIds, null, 2);
  })
  .catch((error) =>{
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);

routGetActions.addEventListener('click', function(){
  api.routine.get(routine.id)
  .then((data) => {
    resultTextArea.innerHTML = JSON.stringify(data.routine.actions, null, 2);
  })
  .catch((error) =>{
    resultTextArea.innerHTML = 'Request failed: ' + error;
  });
}, false);



//esto tiene que estar acá, sino, se rompe todo
}, false);
