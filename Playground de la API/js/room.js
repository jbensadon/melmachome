api.model = api.model || {};

/* Cada room va a tener (en meta) un array de los
** dispositivos que tiene, ocupa más pero es
** más eficiente */

api.model.room = class {
  constructor(id, name, meta) {
    if (id) {
      this.id = id;
    } else {
      delete(this.id);
    }
    this.name = name;
    this.meta = meta;
  }
}
