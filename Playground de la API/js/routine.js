api.model = api.model || {};

api.model.routine = class {
  constructor(name, actions, meta){
    if(!name) return null;
    this.name = name;
    this.actions = actions;
    this.meta = meta;
  }
}
