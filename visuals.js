window.addEventListener('load', function()
{
	var navBarButtons = document.getElementsByClassName("nav-bar-button");
	var switches = document.getElementsByClassName("switch-input");
	var sliders = document.getElementsByClassName("slider");
	var dropdowns = document.getElementsByClassName("form-control");
	var color_palettes = document.getElementsByClassName("color-palette");
	var routine_mode = document.getElementById('routine_mode_notification');
	if(routine_mode != null)
	{
		routine_mode.style.display = "none";
		if(localStorage.getItem('routinesteps')){
			routine_mode.style.display = "block";
		}
	}
	// Imprime estado de switches
	for(var i=0; i < switches.length; i++)
	{
		switches[i].onclick = function(){
			//console.log("Switch became " +this.checked);
		}
	}

	// Imprime estado de sliders
	for (var i=0; i<sliders.length;i++)
	{
		sliders[i].oninput = function() {
			//console.log(this.id +" : " +this.value +"°C");
		};
	}

	// Imprime dropdowns
	for(var i=0; i < dropdowns.length; i++)
	{
		dropdowns[i].oninput = function(){
			//console.log("Este selector ahora es: " +this.value);
		};
	}

	// Imprime Color
	for(var i=0; i < color_palettes.length; i++)
	{
		color_palettes[i].oninput = function(){
			//console.log("Color: " +this.value);
		};
	}

	// NavBar
	for (var i=0;i<navBarButtons.length;i++)
	{
		navBarButtons[i].onmouseover = function(){
			this.style.background ='blue';
		};

		navBarButtons[i].onmouseout = function(){
			this.style.background ='black';
		};
	}
});

//ALARM
var alarm_turnon1 = document.getElementById("alarm-turnon1");
var alarm_turnon2 = document.getElementById("alarm-turnon2");
var alarm_turnoff = document.getElementById("alarm-turnoff");
var alarm_status = document.getElementById("alarm_status");
if(alarm_turnon1 != null)
{
	//Hago de cuenta que estaba desactivada
	alarm_turnon1.style.display = 'inline';
	alarm_turnon2.style.display = 'inline';
	alarm_turnoff.style.display = 'none';

	alarm_turnon1.onclick = function(){
		//ACTIVAR Y ELIMINAR BOTONES DE ENCENDER
		alarm_turnon1.style.display = 'none';
		alarm_turnon2.style.display = 'none';
		alarm_turnoff.style.display = 'inline';
		alarm_status.innerHTML = "ACTIVADA"
		alarm_status.classList.remove("text-muted");
		alarm_status.classList.add("text-primary");
	};

	alarm_turnon2.onclick = function(){
		//ACTIVAR Y ELIMINAR BOTONES DE ENCENDER
		alarm_turnon1.style.display = 'none';
		alarm_turnon2.style.display = 'none';
		alarm_turnoff.style.display = 'inline';
		alarm_status.innerHTML = "ACTIVADA"
		alarm_status.classList.remove("text-muted");
		alarm_status.classList.add("text-primary");
	};

	alarm_turnoff.onclick = function(){
		//DESACTIVAR Y ELIMINAR BOTON DE APAGAR
		alarm_turnon1.style.display = 'inline';
		alarm_turnon2.style.display = 'inline';
		alarm_turnoff.style.display = 'none';
		alarm_status.innerHTML = "Desactivada"
		alarm_status.classList.remove("text-primary");
		alarm_status.classList.add("text-muted");
	};
}

// CHANGING TIMER BUTTONS
var counting = true;
var timer_button = document.getElementById("timer-button");
if(timer_button != null)
{
	timer_button.onclick = function() {
		if(this.innerHTML === "Continuar")
		{
			console.log("Turned on Timer");
			this.innerHTML = "Detener";
			counting = true;
		}
		else {
			console.log("Turned off Timer");
			this.innerHTML = "Continuar";
			counting = false;
		}
	};
}

// TIMER - El input solo funciona en Firefox!
var seconds_left = document.getElementById("seconds_for_timer");
if(seconds_left != null)
{
	seconds_left = seconds_left.value;
	var days = 0;
	var hours = 0;
	var minutes = 0;
	var seconds = 0;
	if(seconds_left > 0)
	{
		days = Math.floor(seconds_left / (3600 * 24));
		hours = Math.floor((seconds_left % (3600 * 24)) / (3600));
		minutes = Math.floor((seconds_left % (3600)) / 60);
		seconds = Math.floor(seconds_left%60);
	}
	else
	timer_button.style.display = 'none';
	// Update the count down every 1 second
	var x = setInterval(function() {
		console.log("Hola");
		if(counting && seconds_left > 0)
		{
			seconds_left--;

			document.getElementById("timer_screen").classList.remove("text-muted");
			document.getElementById("timer_screen").classList.add("text-primary");
			days = Math.floor(seconds_left / (3600 * 24));
			hours = Math.floor((seconds_left % (3600 * 24)) / (3600));
			minutes = Math.floor((seconds_left % (3600)) / 60);
			seconds = Math.floor(seconds_left%60);
		}
		// Display the result in the element with id="timer_screen"
		document.getElementById("timer_screen").innerHTML = days + "d " + hours + "h "
		+ minutes + "m " + seconds + "s ";

		// If the count down is finished or stopped, set color to gray
		if (seconds_left == 0) {
			clearInterval(x);
			document.getElementById("timer_screen").classList.add("text-muted");
			document.getElementById("timer_screen").classList.remove("text-primary");
		}
		else if(!counting)
		{
			document.getElementById("timer_screen").classList.add("text-muted");
			document.getElementById("timer_screen").classList.remove("text-primary");
		}
	}, 1000);

	var reload_timer = document.getElementById("reload-button")
	reload_timer.onclick = function(){window.location.reload(false);};
}

//Boton para volver atras según historial
function goBack(){
	window.history.back();
}

//función para el searchbar
function filter(element, listName) {
	var value = $(element).val();
	$('#'+ listName + ' > li').each(function() {
		if(this.id != "createBtn"){
			if ($(this).text().toUpperCase().search(value.toUpperCase()) > -1) {
				$(this).show();
			}else{
				$(this).hide();
			}
		}
	});
}

function filterDevices(element) {
	var value = $(element).val();
	$("#devices-grid > div").each(function() {
		if( (this.childNodes[0]).id != "addDeviceBtn" ){
			if ($(this).text().toUpperCase().search(value.toUpperCase()) > -1) {
				$(this).show();
			}else{
				$(this).hide();
			}
		}
	});
}

//hardcodeando: lo pongo acá así lo pueden acceder todos
function cancelRoutineMode(){
	//desactivar modo edición rutina
	localStorage.removeItem('routinesteps');
	//indicar que no agregamos nada
	localStorage.removeItem('finishAdding');
	//terminar de limpiar lo que agregamos
	localStorage.removeItem('actionList');
	//volver
	window.location = localStorage.getItem('routinePage');
}
