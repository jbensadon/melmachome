api.model = api.model || {};

/* necesita id del dispositivo, nombre,
** meta y room para saber en donde esta*/
api.model.device = class {
  constructor(id, name, meta){
    if(id){
      this.id = id;
    }else{
      delete(this.id);
    }
    this.name = name;
    this.meta = meta;
  }
}
