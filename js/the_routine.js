//este objeto va a estar guardado dentro del navegador
var currentRoutine = {
	name : null,
	id: null,
	actions:[]
};

var title;
var routineName;
var stepsList;
var backBtn;
var quitModal;
var cancelBtn;
var quitBtn;
var sortable;
var indexArray, tempArray = [];
var routineID;
var isNewRoutine;

window.addEventListener('load', function()
{
	backBtn = document.getElementById("routine-back-button");
	quitModal = document.getElementById("quitModal");
	quitModal.style.display = "none";
	backBtn.addEventListener('click', function(){
		quitModal.style.display = "block";
		cancelBtn = document.getElementById('cancel-button');
		quitBtn = document.getElementById('quit-button');
		cancelBtn.addEventListener('click', function(){
			quitModal.style.display = "none";
		});
		quitBtn.addEventListener('click', function(){
			cancelRoutineCreation();
			window.location.href = "routines.html";
		});
		window.onclick = function(event) {
			if (event.target == quitModal)
				quitModal.style.display = "none";
		}
	});
	var routineTitle = document.getElementById('routineTitle');
	stepsList = document.getElementById('stepsList');
	title = window.location.href.replace(/%20/g," ");
	title = title.split('#');
	routineName = title[1];
	routineID = title[2];
	routineTitle.innerHTML = routineName;
	currentRoutine.name = routineName;
	/*actualizamos la referncia del objeto. Si no se creó, se crea y se codifica
	**si ya estaba creado, se parsea la referncia del que estaba.
	**Hay que tener en cuenta que esto se crea en load, por lo que si alguien
	**después cancela la creación de una rutina clickeando en cualquier otro lado
	**el objeto se guardó igual, no hay problema porque después se sobreescribiría
	**pero podría ser un punto de explotación si se pudiera acceder a él*/
	if(localStorage.getItem('currentRoutine') == null){
		localStorage.setItem('currentRoutine', JSON.stringify(currentRoutine));
	}else{
		currentRoutine = localStorage.getItem('currentRoutine');
		currentRoutine = JSON.parse(currentRoutine);
	}
	//actualizar el objeto si se añadió un paso
	if(localStorage.getItem('finishAdding')){
		localStorage.removeItem('finishAdding');
		var info = localStorage.getItem('actionList').split(',');
		currentRoutine.actions.push({room:info[0], device:info[1], deviceID:info[2],
			actionName:info[3], actionValue:info[4]});
		localStorage.setItem('currentRoutine', JSON.stringify(currentRoutine));
	}

	//llamar a la api para completar lo que falta
	api.routine.get(routineID).then((data) => {
		if(data.routine != undefined)
		{
			isNewRoutine = false;
			var action;
			var meta;
			currentRoutine.name = data.routine.name;
			currentRoutine.id = data.routine.id;

			for(var i = 0; i < data.routine.actions.length; i++){
				action = data.routine.actions[i];
				meta = action.meta.replace('}', "");
				meta = meta.substr(1);
				meta = meta.split(',');
				currentRoutine.actions.push({room:meta[0], device:meta[1],
				deviceID:action.deviceId, actionName:action.actionName,
				actionValue: action.params[0]});
			}
			populateListFromObject(currentRoutine);
		}
	})
	//si falla el get es porque no fue creada
	.catch((error) => {
		isNewRoutine = true;
		populateListFromObject(currentRoutine);
	});

	//creando la lista que permite drag and drop
	sortable = new Sortable(stepsList, {
		//sort es el método que recive el evento de cambio de la lista
		onSort: function (e) {
			var items = e.to.children;
			indexArray = [];
			for (var i = 0; i < items.length; i++) {
				indexArray.push($(items[i]).data('key'));
			}
		}
	});
});


function changeRoutineName(){
	//abrir modal
	//var newName = ..
	//currentRoutine.name = newName;
	//updateRoutine();
	//va a llevar a routines, lo ideal sería que recargue la página pero no sé si llegamos con el tiempo
}

//-------------------------------------------------------------------------STEPS
//funcion para preparar las variables para poder añadir un paso a la rutina
function activeAddRoutineSteps(){
	//indicar que estamos en modo edicion
  localStorage.setItem('routinesteps', true);
	//indicar si se terminó de agregar un paso
	localStorage.setItem('finishAdding', false);
  //pagina de la rutina
  localStorage.setItem('routinePage', location.href.toString());
  //habitacion, dispositivo, acción;
  localStorage.setItem('actionList', "");
  //ir a rooms
  window.location = "rooms.html";
}

//guardar y eliminar
function finishAddingSteps(){
	if(currentRoutine.actions.length != 0){

		if(isNewRoutine){
			//borramos lo que queda
			localStorage.removeItem('routinePage');
			localStorage.removeItem('actionList');
			//ordenamos la lista
			if(indexArray != undefined){
				rearrangeActionArray();
			}
			connectToApi();
		}else{
			//ordenamos la lista
			if(indexArray != undefined){
				rearrangeActionArray();
			}
			updateRoutine();
		}
	}else{
		//mostrar cartelito "no se pueden guardar rutinas vacías";
	}
}


//ordenar la lista en base a los indices modificados por le drag & drop
function rearrangeActionArray(){
	if(indexArray != undefined){
		for(var i = 0; i < indexArray.length; i++){
			tempArray.push(currentRoutine.actions[indexArray[i]]);
		}
		currentRoutine.actions = tempArray;
	}
	//falta botón para confirmar las rotaciones, así no los guarda todo el tiempo
}

//para indicar el elemento a borrar, se borra cuando se updatea
function deleteItem(obj){

	obj_id = parseInt(obj.id.split("-")[1]);
	//guardamos el orden
	if(indexArray != undefined){
		rearrangeActionArray();
		obj_id = indexArray.indexOf(obj_id);
	}
	//borramos el elemento
	currentRoutine.actions.splice(obj_id, 1);
	localStorage.setItem('currentRoutine', JSON.stringify(currentRoutine));

	//cambiar el color del li a rojo
	var parent = obj.parentElement.parentElement.parentElement;
	parent.setAttribute("class", "row room-label bg-danger");
}

//----------------------------------------------------------------------CANCEL'S

function cancelRoutineMode(){
	//desactivar modo edición rutina
	localStorage.removeItem('routinesteps');
	//indicar que no agregamos nada
	localStorage.removeItem('finishAdding');
	//terminar de limpiar lo que agregamos
	localStorage.removeItem('actionList');
	//volver
	window.location = localStorage.getItem('routinePage');
}

//ver si directamente no se puede hacer localStorage.clear()
function cancelRoutineCreation(){
	//destruir la rutina actual
	if(localStorage.getItem('currentRoutine')){
		localStorage.removeItem('currentRoutine');
	}

	localStorage.removeItem('routinePage');
	localStorage.removeItem('actionList');
	//destruir el sortable
	sortable.destroy();
	window.location = "routines.html";
}

//----------------------------------------------------------------CONNECT TO API
//funcion para convertir las acciones en un string para la api
function actionsToText(){
	var text = "";
	var action;
	text += '['
	for(var i = 0; i < currentRoutine.actions.length; i++){
		action = currentRoutine.actions[i];
		if(text[text.length-1] == '}'){
			text += ',';
		}
		if(action.room != null)
		{
			text += '{';
			text += '\"deviceId\": ' + '"' +action.deviceID + '" , ';
			text += '\"actionName\": ' + '\"' +action.actionName + '\", ';
			text += '\"params\": ["' + action.actionValue.toString() + '"], ';
			text +=  '\"meta\": \"{' + action.room + ',' + action.device + '}\"';
			text += '}';
		}
	}
	text += ']'
	return text;
}

//add element
function connectToApi(){
		actionText = actionsToText();
		var routine = {
			"name": currentRoutine.name,
			"actions": actionText,
			"meta": '{}'
		}
		console.log("routineID");
		api.routine.add(routine)
	  .then((data) => {
			routineID = data.routine.id;
			console.log(routineID);
			localStorage.removeItem('currentRoutine');
			window.location = "routines.html#"+currentRoutine.name+"#"+routineID;
			//no se si funciona o no pero por las dudas no se toca
	    resultTextArea.innerHTML = JSON.stringify(data, null, 2);
	  })
	  .catch((error) => {
	    resultTextArea.innerHTML = 'Request failed: ' + error;
	  });

}

function updateRoutine(){
	actionText = actionsToText();
	var routine = {
		"name": currentRoutine.name,
		"actions": actionText,
		"meta": '{}'
	};
	localStorage.removeItem('currentRoutine');
	api.routine.modify(routineID, routine)
	.then((data) => {
		//resultTextArea.innerHTML = JSON.stringify(data, null, 2);
		localStorage.removeItem('currentRoutine');
		window.location = "routines.html#"+currentRoutine.name+"#"+routineID;
	})
	.catch((error) => {
		console.log(error);
		resultTextArea.innerHTML = 'Request failed: ' + error;
	});
}

function deleteRoutine(){
	api.routine.delete(routineID)
	.then((data) => {
		window.location = "routines.html";
	});

}

//--------------------------------------------------------------------------HTML

var elemHTML = '\
<li data-key="{routineStepID}">\
	<div class="switch-separator"></div>\
  <div class="row room-label bg-secondary">\
	<div class="routine-step-number-box routine-step-label-text">\
		<p>{routineStepID}</p>\
	</div>\
	<div class="switch-separator"></div>\
	<div class="routine-step-name-box routine-step-label-text">\
  	<p>{routineStep}</p>\
	</div>\
	<div class="switch-separator"></div>\
	<div class="routine-step-trash-box">\
	<a class="clickeable">\
		<img id=\'button-{routineStepID}\' width="30px;" height="30px;" onclick="deleteItem(this)" src="img/trash_white.png">\
	</a>\
	</div>\
  </div>\
</li>';

function populateListFromObject(currentRoutine){
	var action;
	var elemText = document.getElementById("routineStepText");
	var temp = '';
	for(var i = 0; i < currentRoutine.actions.length; i++){
			action = currentRoutine.actions[i];
			if(action.room != null){
				elemText = action.room + " > " + action.device + " > " + action.actionName + " > " + action.actionValue;
				temp = elemHTML.replace(/{routineStep}/, elemText);
				temp = temp.replace(/{routineStepID}/g, i);
				stepsList.innerHTML += temp;
		}
	}
}
