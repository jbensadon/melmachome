var deviceTypeIconMap = new Map();
deviceTypeIconMap.set("im77xxyulpegfmv8", "dark_device_oven.svg");
deviceTypeIconMap.set("rnizejqr2di0okho", "dark_device_fridge.svg");
deviceTypeIconMap.set("go46xmbqeomjrsjr", "dark_device_lamp.svg");
deviceTypeIconMap.set("lsf78ly0eqrjbz91", "dark_device_door.svg");
deviceTypeIconMap.set("eu0v2xgprrhhg41g", "dark_device_blind.svg");
deviceTypeIconMap.set("li6cbv5sdlatti0j", "dark_device_ac.svg");

var deviceTypeLinkMap = new Map();
deviceTypeLinkMap.set("im77xxyulpegfmv8", "device_oven.html");
deviceTypeLinkMap.set("rnizejqr2di0okho", "device_fridge.html");
deviceTypeLinkMap.set("go46xmbqeomjrsjr", "device_lights.html");
deviceTypeLinkMap.set("lsf78ly0eqrjbz91", "device_door.html");
deviceTypeLinkMap.set("eu0v2xgprrhhg41g", "device_blinds.html");
deviceTypeLinkMap.set("li6cbv5sdlatti0j", "device_ac.html");

var addDeviceElementHTML = '\
<div class="col-sm-4">\
<div id="addDeviceBtn" class="card-inverse bg-inverse text-xs-center p-t-2">\
<div class="card card-title" style="height:198px">\
<img stlye="text-align:center" src="img/add_icon.png" style="display: block;margin: auto;" width="150px" height="150px;">\
<h6 class="text-dark">Agregar nuevo dispositivo</h6>\
</div>\
</div>\
</div>\
';

var deviceElementHTML = '\
<div class="col-sm-4">\
<a href="{deviceLink}">\
<div class="card-inverse bg-inverse text-xs-center p-t-2">\
<div class="card card-title" style="height:198px">\
<img stlye="text-align:center" src="img/device_icons/{imgSrc}" style="display: block;margin: auto;" width="150px" height="150px;">\
<h6 class="text-dark">{deviceName} ({roomName})</h6>\
</div>\
</div>\
</div>\
</a>\
';

var errorMsgHTML = '\
<div id="modalError" class="alert alert-danger">\
<h0 id="modalErrorText">{errorMsg}</h0>\
</div>';

var deviceRoomOptionHTML = '\
<option value="{roomId}">{roomName}</option>\
';

var deviceNameList = [];
var roomsList;
var errorcontainer;

window.addEventListener('load', function()
{
  var deviceRoomSelectElement = document.getElementById('deviceRoomInput');
  errorcontainer = document.getElementById('errors');
  errorcontainer.style.display = "none";
  var deviceGridElement = document.getElementById('devices-grid');

  //agarrar la lista de habitaciones
  api.room.getAll().then((data) => {
    roomsList = data;
    if (roomsList.rooms != undefined){
      deviceRoomSelectElement.innerHTML = generateOptionsHTMLFromRoomsList(roomsList);
      //lo agrego acá para que aparezca apenas carga la pag
      if(roomsList.rooms.length == 0){
        errorMsg("Por favor, primero agregue una habitación.");
        document.getElementById("modalError").setAttribute("class", "alert alert-warning");
        return;
      }

      // api.device.getAll().then((data) => {
      //   devicesList = data;
      //   if(localStorage.getItem('routinesteps'))
      //   {
      //     deviceGridElement.innerHTML = "";
      //   }
      //   else{
      //     deviceGridElement.innerHTML = addDeviceElementHTML;
      //   }
      //   if (devicesList.devices != undefined){
      //     deviceGridElement.innerHTML += generateDeviceHTMLFromDevicesList(devicesList, roomsList);
      //     addDeviceNamesToList(data.devices);
      //   }
      //   if(!(localStorage.getItem('routinesteps')))
      //   {
      //     setupAddDevicePopup();
      //   }
      // })
      // .catch((error) => {
      //   deviceGridElement.innerHTML = 'Request failed: ' + error;//TODO: ERROR HANDLING
      // });

    }
  })
  .catch((error) => {
    deviceRoomSelectElement.innerHTML = 'Request failed: ' + error;//TODO: ERROR HANDLING
  });
  api.device.getAll().then((data) => {
        devicesList = data;
        if(localStorage.getItem('routinesteps'))
        {
          deviceGridElement.innerHTML = "";
        }
        else{
          deviceGridElement.innerHTML = addDeviceElementHTML;
        }
        if (devicesList.devices != undefined){
          deviceGridElement.innerHTML += generateDeviceHTMLFromDevicesList(devicesList, roomsList);
          addDeviceNamesToList(data.devices);
        }
        if(!(localStorage.getItem('routinesteps')))
        {
          setupAddDevicePopup();
        }
      })
      .catch((error) => {
        deviceGridElement.innerHTML = 'Request failed: ' + error;//TODO: ERROR HANDLING
      });

});

function addDeviceNamesToList(deviceList){
  for (i = 0; i < deviceList.length; i++){
    deviceNameList.push(deviceList[i].name);
  }
}

function generateDeviceHTMLFromDevicesList(list, roomsList){
  var result = '';
  var i;
  for (i = 0; i < list.devices.length; i++)
  {
    var roomId = JSON.parse(list.devices[i].meta).room;
    var temp = deviceElementHTML.replace(/{deviceLink}/, deviceTypeLinkMap.get(list.devices[i].typeId) + "#" + list.devices[i].id +"#" +roomId);
    temp = temp.replace(/{imgSrc}/, deviceTypeIconMap.get(list.devices[i].typeId));
    for(var j=0; j<roomsList.rooms.length; j++)
    {
      if(roomsList.rooms[j].id == roomId)
        temp = temp.replace(/{roomName}/, roomsList.rooms[j].name);
    }
    result += temp.replace(/{deviceName}/, list.devices[i].name);
  }
  return result;
}

function generateOptionsHTMLFromRoomsList(list){
  var result = '';
  var i;
  for (i = 0; i < list.rooms.length; i++){
    var temp = deviceRoomOptionHTML.replace(/{roomName}/, list.rooms[i].name);
    result += temp.replace(/{roomId}/, list.rooms[i].id);
  }
  return result;
}

function setupAddDevicePopup(){
  var addDeviceBtn = document.getElementById("addDeviceBtn");

  var modal = document.getElementById('myModal');
  var closeBtn = document.getElementsByClassName("close")[0];
  var createBtn = document.getElementById("create-button");

  var deviceNameInputElem = document.getElementById("deviceNameInput");
  var deviceRoomSelectElem = document.getElementById("deviceRoomInput");
  var deviceTypeSelectElem = document.getElementById("deviceTypeInput");
  deviceNameInputElem.value = '';


  addDeviceBtn.onclick = function() {
    modal.style.display = "block";
  }

  closeBtn.onclick = function() {
    modal.style.display = "none";
  }

  /*----------------------------------------------------------------------------
  Tipos de errores:
  -Nombre vacío
  -Nombre menor a 3 caracteres
  -ya existe un disp con el mismo nombre
  -No hay habitaciones cargadas
  */


  createBtn.onclick = function(){
    if(roomsList.rooms.length == 0){
        errorMsg("Por favor, primero cree una habitación.")
        return;
    }
    else if (deviceNameInputElem.value == ""){
      errorMsg("Por favor, introduzca un nombre para el dispositivo.");
      return;
    }
    else if(deviceNameInputElem.value.length < 3){
      errorMsg("Por favor, introduzca un nombre con al menos 3 caracteres.");
      return;
    }
    else if (deviceNameList.includes(deviceNameInputElem.value)){
      errorMsg("Ya existe un dispositivo con ese nombre. Por favor, escoja otro.");
      return;
    }
    else if(isInvalidString(deviceNameInputElem.value))
    {
      errorMsg("El nombre ingresado contiene caracteres inválidos. Por favor, sólo utilice letras, números y espacios. No se admiten tildes o la letra Ñ.");
      return;
    }
    createBtn.style = "background:grey;";
    switch(deviceTypeSelectElem.options[deviceTypeSelectElem.selectedIndex].value){
      case "im77xxyulpegfmv8": //Oven
        var deviceMeta = oven_meta;
        break;
      case "rnizejqr2di0okho": //Fridge
        var deviceMeta = fridge_meta;
        break;
      case "go46xmbqeomjrsjr": //Lights
        var deviceMeta = lamp_meta;
        break;
      case "lsf78ly0eqrjbz91": //Door
        var deviceMeta = door_meta;
        break;
      case "eu0v2xgprrhhg41g": //Blinds
        var deviceMeta = blinds_meta;
        break;
      case "li6cbv5sdlatti0j": //Air Conditioner
        var deviceMeta = ac_meta;
        break;
    }
    deviceMeta.room = deviceRoomSelectElem.options[deviceRoomSelectElem.selectedIndex].value;
    var newDevice = {
      "typeId": deviceTypeSelectElem.options[deviceTypeSelectElem.selectedIndex].value,
      "name": deviceNameInputElem.value,
      "meta": JSON.stringify(deviceMeta)
    };
    api.device.add(newDevice)
    .then((data) => {
      api.device.setDeviceRoom(data.device.id, deviceRoomSelectElem.options[deviceRoomSelectElem.selectedIndex].value)
      .then((data) => {
        window.location.reload(false);
      })
      .catch((error) => {
        document.write(error + " - While adding device to room.");//TODO: ERROR HANDLING
      });

    })
    .catch((error) => {
      document.write(error + " - While creating device");//TODO: ERROR HANDLING
    });
  }


  // cuando aprieta fuera de pop-up se cierra
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
      errorcontainer.style.display = "none";
    }
  }
}


function errorMsg(msg){
  errorcontainer.style.display = "block";
  var tmp = errorMsgHTML.replace(/{errorMsg}/, msg);
  errorcontainer.innerHTML = tmp;
  document.getElementById("modalError").setAttribute("class", "alert alert-danger");
}

function isInvalidString(string){
  for(var i=0; i<string.length; i++)
  {
    if(!((string[i]>='a' && string[i]<='z') || (string[i]>='A' && string[i]<='Z') || string[i]==' ' || (string[i]>='0' && string[i]<='9')))
      return true;
  }
  return false;
}
