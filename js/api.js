var api = class {
  static get baseUrl() {
    return "http://127.0.0.1:8080/api/";
  }

  static get timeout() {
    return 60 * 1000;
  }

  static fetch(url, init) {
    return new Promise(function(resolve, reject) {
      var timeout = setTimeout(function() {
        reject(new Error('Time out'));
      }, api.timeout);

      fetch(url, init)
      .then(function(response) {
        clearTimeout(timeout);
          if (!response.ok)
            reject(new Error(response.statusText));

          return response.json();
      })
      .then(function(data) {
          resolve(data);
      })
      .catch(function(error) {
          reject(error);
      });
    });
  }
}

api.room = class {
  static get url() {
    return api.baseUrl + "rooms/";
  }

  static add(room) {
   return api.fetch(api.room.url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(room)
    });
  }

  static modify(roomId, roomInfo) {
    return api.fetch(api.room.url + roomId, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(roomInfo)
    });
  }

  static delete(id) {
    return api.fetch(api.room.url + id, {
      method: 'DELETE',
    });
  }

  static get(id) {
    return api.fetch(api.room.url + id);
  }

  static getAll() {
    return api.fetch(api.room.url);
  }

  static getDevices(roomID)
  {
    return api.fetch(api.room.url + roomID + "/devices/");
  }
}

api.device = class {
  static get url() {
    return api.baseUrl + "devices/";
  }

  static add(device) {
   return api.fetch(api.device.url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(device)
    });
  }

  static modify(deviceId, info) {
    return api.fetch(api.device.url + deviceId, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      deviceId: deviceId,
      body: info
    });
  }

  static setDeviceRoom(deviceId, roomId){
    return api.fetch(api.device.url + deviceId + '/rooms/' + roomId, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      deviceId: deviceId,
      roomId: roomId,
      body: {}
    });
  }

  static deleteFromRoom(deviceId, roomId)
  {
    return api.fetch(api.device.url + deviceId + '/rooms/', {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      deviceId: deviceId
    });
  }

  static action(id, actionName, body){
    return api.fetch(api.device.url + id + '/' + actionName, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      deviceId: id,
      actionName: actionName,
      body: body
    });
  }

  static delete(id) {
    return api.fetch(api.device.url + id, {
      method: 'DELETE',
    });
  }

  static get(id) {
    return api.fetch(api.device.url + id);
  }

  static getAll() {
    return api.fetch(api.device.url);
  }
}

api.deviceType = class {
  static get url() {
    return api.baseUrl + "devicetypes/";
  }

  static get(id) {
    return api.fetch(api.deviceType.url + id);
  }

  static getAll() {
    return api.fetch(api.deviceType.url);
  }
}

api.routine = class{
  static get url() {
    return api.baseUrl + "routines/";
  }

  static add(routine) {
   return api.fetch(api.routine.url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(routine)
    });
  }

  static modify(routineId, routine) {
    return api.fetch(api.routine.url + routineId, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(routine)
    });
  }

  static delete(id) {
    return api.fetch(api.routine.url + id, {
      method: 'DELETE',
    });
  }

  static get(id) {
    return api.fetch(api.routine.url + id);
  }

  static getAll() {
    return api.fetch(api.routine.url);
  }

  static execute(routineId){
    return api.fetch(api.routine.url + routineId + "/execute", {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    });
  }
}
