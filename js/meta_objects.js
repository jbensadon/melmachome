var oven_meta = {
	"room": "room",
	"state": false,
	"temperature": 100,
	"heatSource": "conventional",
	"grillMode": "off",
	"convectionMode": "off"
};

var fridge_meta = {
	"room": "room",
	"fridgeTemperature": 4,
	"freezerTemperature": -16,
	"mode": "default"
};

var lamp_meta = {
	"room": "room",
	"state": false,
	"intensity": 100,
	"color": "#FFFFFF",
};

var door_meta = {
	"room": "room",
	"state": false,
	"locked": false,
};

var blinds_meta = {
	"room": "room",
	"pulled": false,
};

var ac_meta = {
	"room": "room",
	"state": false,
	"temperature": 24,
	"mode": "fan",
	"verticalAngle": "auto",
	"horizontalAngle": "auto",
	"fanSpeed": "auto"
};