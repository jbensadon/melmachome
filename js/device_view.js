var errorMsgHTML = '\
<div id="modalError" class="alert alert-danger">\
<h0 id="modalErrorText">{errorMsg}</h0>\
</div>';

var deviceRoomOptionHTML = '\
<option value="{roomId}">{roomName}</option>';

var deviceTypeIdMap = new Map();
deviceTypeIdMap.set("device_oven.html", "im77xxyulpegfmv8");
deviceTypeIdMap.set("device_fridge.html", "rnizejqr2di0okho");
deviceTypeIdMap.set("device_lights.html", "go46xmbqeomjrsjr");
deviceTypeIdMap.set("device_door.html", "lsf78ly0eqrjbz91");
deviceTypeIdMap.set("device_blinds.html", "eu0v2xgprrhhg41g");
deviceTypeIdMap.set("device_ac.html", "li6cbv5sdlatti0j");

var editModal;
var delModal;
var editBtn;
var trashBtn;
var cancelBtn;
var deleteBtn;
var deviceId;
var deviceTypeId;
var roomId;
var deviceTitle, deviceName, roomName;
var backBtn;
var roomsList = [];
var deviceNameList = [];
var errorcontainer;
var deviceMeta;
var regNavBar;
var routineNavBar;

window.addEventListener('load', function(){
  editModal = document.getElementById('editModal');
  delModal = document.getElementById('deleteModal');
  editBtn = document.getElementById('edit-button');
  trashBtn = document.getElementById("trash-button");
  cancelBtn = document.getElementById("cancel-button");
  deleteBtn = document.getElementById("delete-button");
  deviceId = window.location.href.split('#')[1];
  deviceTypeId = deviceTypeIdMap.get(window.location.href.split('/').pop().split('#')[0]);
  roomId = window.location.href.split('#')[2];
  deviceTitle = document.getElementsByClassName("screen-title")[0];
  backBtn = document.getElementById("device-back-button");
  var controlsContainerElement = document.getElementById("controls-container");

  if (deviceId == undefined){
    controlsContainerElement.innerHTML = "URL incorrecta. Por favor, vuelva atrás y seleccione un dispositivo nuevamente.";
    return;
  }

  regNavBar = document.getElementById('regular-tabs');
  routineNavBar = document.getElementById('in-routine-tabs');
  if(localStorage.getItem('routinesteps'))
  {
    regNavBar.style.display = "none";
    routineNavBar.style.display = "block";
    editBtn.style.display = "none";
    trashBtn.style.display = "none";
  }
  else{
    regNavBar.style.display = "block";
    routineNavBar.style.display = "none";
  }

  setUpPageForAllDeviceTypes();
});

function setUpPageForAllDeviceTypes(){
  errorcontainer = document.getElementById('errors');
  api.device.getAll()
  .then((data) => {
    devicesList = data;
    if (devicesList.devices != undefined){
      addDeviceNamesToList(data.devices);
    }
    setupEditDevicePopup();
  });
  api.room.get(roomId).then((data) => {
    if(data.room != undefined)
    {
      roomName = data.room.name;
      api.device.get(deviceId).then((data) => {
        if(data.device != undefined)
        {
          deviceMeta = JSON.parse(data.device.meta);
          //console.log(JSON.stringify(deviceMeta));
          deviceName = data.device.name;
          deviceTitle.innerHTML = deviceName +" (" + roomName +")";
          generateControlPanelForDeviceType(deviceId);
        }
      })
    }
  });
  var deviceRoomSelectElement = document.getElementById('deviceRoomInput');
  //agarrar la lista de habitaciones
  api.room.getAll()
  .then((data) => {
    roomsList = data;
    if (roomsList.rooms != undefined)
      deviceRoomSelectElement.innerHTML = generateOptionsHTMLFromRoomsList(roomsList);
    deviceRoomSelectElement.selectedIndex = -1;
    for(var i=0; deviceRoomSelectElement.selectedIndex == -1 && i<deviceRoomSelectElement.options.length; i++)
    {
      if(deviceRoomSelectElement.options[i].value == roomId)
        deviceRoomSelectElement.selectedIndex = i;
    }
  })
  .catch((error) => {
    deviceRoomSelectElement.innerHTML = 'Request failed: ' + error;//👏👏👏👏👏👏
  });
  setupEditDevicePopup();

  backBtn.onclick = function(){
    window.history.back();
  }

  trashBtn.onclick = function() {
    delModal.style.display = "block";
    window.onclick = function(event){
      if (event.target == delModal)
        delModal.style.display = "none";
    }
  }

  cancelBtn.onclick = function() {
    delModal.style.display = "none";
  }

  deleteBtn.onclick = function() {
    api.device.delete(deviceId).then((data) => {
      window.location.href = "room_view.html" +"#" +roomId;
    });
  }
}

function addDeviceNamesToList(deviceList){
  for (i = 0; i < deviceList.length; i++){
    deviceNameList.push(deviceList[i].name);
  }
}

function generateOptionsHTMLFromRoomsList(list){
  var result = '';
  var i;
  for (i = 0; i < list.rooms.length; i++){
    var temp = deviceRoomOptionHTML.replace(/{roomName}/, list.rooms[i].name);
    result += temp.replace(/{roomId}/, list.rooms[i].id);
  }
  return result;
}

function setupEditDevicePopup(){
  var modal = document.getElementById('editModal');
  var closeBtn = document.getElementsByClassName("close")[0];
  var createBtn = document.getElementById("create-button");

  var deviceNameInputElem = document.getElementById("deviceNameInput");
  api.device.get(deviceId).then((data) => {
    if(data.device != undefined)
      deviceNameInputElem.value = data.device.name;
  });

  editBtn.onclick = function(){
    editModal.style.display = "block";
  }

  closeBtn.onclick = function() {
    editModal.style.display = "none";
  }

  /*----------------------------------------------------------------------------
  Tipos de errores:
  -Nombre vacío
  -Nombre menor a 3 caracteres
  -ya existe un disp con el mismo nombre
  -No hay habitaciones cargadas
  */

  createBtn.onclick = function(){
    if (deviceNameInputElem.value == ""){
      errorMsg("Por favor, introduzca un nombre para el dispositivo.");
      return;
    }
    else if(deviceNameInputElem.value.length < 3){
      errorMsg("Por favor, introduzca un nombre con al menos 3 caracteres.");
      return;
    }
    else if ((deviceNameInputElem.value != deviceTitle.innerHTML.split(' (')[0]) && deviceNameList.includes(deviceNameInputElem.value)){
      errorMsg("Ya existe un dispositivo con ese nombre. Por favor, escoja otro.");
      return;
    }
    else if(isInvalidString(deviceNameInputElem.value))
    {
      errorMsg("El nombre ingresado contiene caracteres inválidos. Por favor, sólo utilice letras, números y espacios. No se admiten tildes o la letra Ñ.");
      return;
    }
    createBtn.style = "background:grey;";
    var deviceRoomSelectElement = document.getElementById('deviceRoomInput');
    api.device.get(deviceId).then((data) =>{
      deviceMeta.room = deviceRoomSelectElement.options[deviceRoomSelectElement.selectedIndex].value;
      var newDevice = {
        "typeId": data.device.typeId,
        "name": deviceNameInputElem.value,
        "meta": JSON.stringify(deviceMeta)
      };
      api.device.deleteFromRoom(deviceId, roomId).then((data) => {
        api.device.setDeviceRoom(deviceId, deviceRoomSelectElement.options[deviceRoomSelectElement.selectedIndex].value).then((data) =>{
          api.device.modify(deviceId, JSON.stringify(newDevice)).then((data)=>{
            window.location.href = "room_view.html#" +deviceRoomSelectElement.options[deviceRoomSelectElement.selectedIndex].value;
          })
        }).catch((error) => {
          document.write(error + " - While editing device");//TODO: ERROR HANDLING;
        })
      });
    });
  }
  window.onclick = function(event) {
    if (event.target == editModal)
      modal.style.display = "none";
  }
}

function generateControlPanelForDeviceType(deviceId){

  switch(window.location.href.split('/').pop().split('#')[0]){
    case ("device_oven.html"):
      var switchLabel = document.getElementById('stateSwitchLabel');
      var dropdownLabel = document.getElementById('stateDropdownLabel');
      var textTemperature = document.getElementById('temp-text');
      var stateSwitch = document.getElementById("stateSwitch");
      var stateDropdown = document.getElementById('stateDropdown');
      var temperatureSlider = document.getElementById("temperatureSlider");
      var heatSourceSelect = document.getElementById("heatSourceSelect");
      var grillModeSelect = document.getElementById("grillModeSelect");
      var convectionModeSelect = document.getElementById("convectionModeSelect");

      stateSwitch.checked = deviceMeta.state;
      temperatureSlider.value = deviceMeta.temperature;
      textTemperature.innerHTML = deviceMeta.temperature + "°C";
      heatSourceSelect.value = deviceMeta.heatSource;
      grillModeSelect.value = deviceMeta.grillMode;
      convectionModeSelect.value = deviceMeta.convectionMode;

      if(localStorage.getItem('routinesteps')){
        dropdownLabel.style.display = "compact";
        switchLabel.style.display = "none";
        stateDropdown.selectedIndex = 0;
        heatSourceSelect.selectedIndex = 0;
        grillModeSelect.selectedIndex = 0;
        convectionModeSelect.selectedIndex = 0;
      }
      else{
        dropdownLabel.style.display = "none";
        switchLabel.style.display = "compact";
      }

      stateSwitch.addEventListener('click', function(){
        if (stateSwitch.checked){
          api.device.action(deviceId, "turnOn", 'true').then((data) => {
            deviceMeta.state = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        else{
          api.device.action(deviceId, "turnOff", 'true').then((data) => {
            deviceMeta.state = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }

        deviceMeta.state = stateSwitch.checked;
        registerDeviceAction(deviceId, deviceMeta);
      });

      stateDropdown.addEventListener('change', function(){
        if(localStorage.getItem('routinesteps')){
          var action;
          if(stateDropdown.selectedIndex == 1){
            action = "turnOn,true";
          }else if(stateDropdown.selectedIndex == 2){
            action = "turnOff,true";
          }
          prepareRoutineStepPackage(action);
        }
      });

      temperatureSlider.addEventListener('mouseup', function(){
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setTemperature,"+ this.value.toString());
        }
        else{
          api.device.action(deviceId, "setTemperature", this.value).then((data) => {
            deviceMeta.temperature = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log(this.id +" : " +this.value + "°C");
      });

      //para mostrar el valor
      temperatureSlider.addEventListener('input', function(){
        textTemperature.innerHTML = this.value +"°C";
      });

      heatSourceSelect.addEventListener('change', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setHeat,"+ this.value.toString());
        }
        else{
          api.device.action(deviceId, "setHeat", this.value).then((data) => {
            deviceMeta.heatSource = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log("Este selector ahora es: " + this.value);
      });

      grillModeSelect.addEventListener('change', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setGrill,"+ this.value.toString());
        }
        else{
          api.device.action(deviceId, "setGrill", this.value).then((data) => {
            deviceMeta.grillMode = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log("Este selector ahora es: " + this.value);
      });

      convectionModeSelect.addEventListener('change', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setConvection,"+ this.value.toString());
        }
        else{
          api.device.action(deviceId, "setConvection", this.value).then((data) => {
            deviceMeta.convectionMode = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log("Este selector ahora es: " + this.value);
      });
      break;

    case ("device_fridge.html"):
      var textFridgeTemp = document.getElementById('fridge-temp-text');
      var textFreezerTemp = document.getElementById('freezer-temp-text');;
      var fridgeTemperatureSlider = document.getElementById("fridgeTempSlider");
      var freezerTemperatureSlider = document.getElementById("freezerTempSlider");
      var modeSelect = document.getElementById("modeSelect");

      fridgeTemperatureSlider.value = deviceMeta.fridgeTemperature;
      textFridgeTemp.innerHTML = deviceMeta.fridgeTemperature + "°C";

      freezerTemperatureSlider.value = deviceMeta.freezerTemperature;
      textFreezerTemp.innerHTML = deviceMeta.freezerTemperature + "°C";

      modeSelect.value = deviceMeta.mode;


      fridgeTemperatureSlider.addEventListener('mouseup', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setTemperature,"+ this.value.toString());
        } else {
          api.device.action(deviceId, "setTemperature", this.value).then((data) => {
            deviceMeta.fridgeTemperature = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log(this.id +" : " +this.value + "°C");
      });

      //solo para mostrar
      fridgeTemperatureSlider.addEventListener('input', function(){
        textFridgeTemp.innerHTML = this.value +"°C";
      });

      freezerTemperatureSlider.addEventListener('mouseup', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setFreezerTemperature,"+ this.value.toString());
        } else {
          api.device.action(deviceId, "setFreezerTemperature", this.value).then((data) => {
            deviceMeta.freezerTemperature = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log(this.id +" : " +this.value + "°C");
      });

      //solo para mostrar
      freezerTemperatureSlider.addEventListener('input', function(){
        textFreezerTemp.innerHTML = this.value +"°C";
      });

      if(localStorage.getItem('routinesteps')){
        modeSelect.selectedIndex = 0;
      }

      modeSelect.addEventListener('change', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setMode,"+ this.value.toString());
        } else {
          api.device.action(deviceId, "setMode", this.value).then((data) => {
            deviceMeta.mode = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log("Este selector ahora es: " + this.value);
      });
      break;

    case ("device_lights.html"):
      var switchLabel = document.getElementById('stateSwitchLabel');
      var dropdownLabel = document.getElementById('stateDropdownLabel');
      var textIntensity = document.getElementById('intensity-text');
      var stateSwitch = document.getElementById("stateSwitch");
      var stateDropdown = document.getElementById('stateDropdown');
      var intensitySlider = document.getElementById("intensitySlider");
      var colorPalette = document.getElementById("colorPalette");

      stateSwitch.checked = deviceMeta.state;
      intensitySlider.value = deviceMeta.intensity;
      textIntensity.innerHTML = deviceMeta.intensity + "%";
      colorPalette.value = deviceMeta.color;

      if(localStorage.getItem('routinesteps')){
        dropdownLabel.style.display = "compact";
        switchLabel.style.display = "none";
        stateDropdown.selectedIndex = 0;
      } else {
        dropdownLabel.style.display = "none";
        switchLabel.style.display = "compact";
      }

      stateSwitch.addEventListener('click', function(){
        if (stateSwitch.checked){
          api.device.action(deviceId, "turnOn", 'true').then((data) => {
            deviceMeta.state = this.checked;
            registerDeviceAction(deviceId, deviceMeta);
          });
        } else {
          api.device.action(checked, "turnOff", 'true').then((data) => {
            deviceMeta.state = this.checked;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log("Switch became " +this.checked);
      });

      stateDropdown.addEventListener('change', function(){
        if(localStorage.getItem('routinesteps')){
          var action;
          if(stateDropdown.selectedIndex == 1){
            action = "turnOn,true";
          }else if(stateDropdown.selectedIndex == 2){
            action = "turnOff,true";
          }
          prepareRoutineStepPackage(action);
        }
      });

      intensitySlider.addEventListener('mouseup', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setBrightness,"+ this.value.toString());
        }
        else{
          api.device.action(deviceId, "setBrightness", this.value).then((data) => {
            deviceMeta.intensity = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log(this.id +" : " +this.value + "°C");
      });

      //solo para mostrar
      intensitySlider.addEventListener('input', function(){
        textIntensity.innerHTML = this.value +"%"
      });

      colorPalette.addEventListener('change', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setColor,"+ this.value.toString());
        }
        else{
          api.device.action(deviceId, "setColor", this.value).then((data) => {
            deviceMeta.color = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
      });
      break;

    case ("device_door.html"):
      var stateSwitchLabel = document.getElementById("stateSwitchLabel");
      var stateDropDownLabel = document.getElementById("stateDropdownLabel");
      var lockSwitchLabel = document.getElementById("lockSwitchLabel");
      var lockDropdown = document.getElementById("lockDropdown");
      var stateSwitch = document.getElementById("stateSwitch");
      var stateDropdown = document.getElementById('stateDropdown');
      var lockSwitch = document.getElementById("lockSwitch");

      stateSwitch.checked = deviceMeta.state;
      lockSwitch.checked = deviceMeta.locked;

      if(localStorage.getItem('routinesteps')){
        stateDropdownLabel.style.display = "compact";
        lockDropdownLabel.style.display = "compact";
        stateSwitchLabel.style.display = "none";
        lockSwitchLabel.style.display = "none";
        stateDropdown.selectedIndex = 0;
        lockDropdown.selectedIndex = 0;
      } else {
        stateDropdownLabel.style.display = "none";
        lockDropdownLabel.style.display = "none";
        stateSwitchLabel.style.display = "compact";
        lockSwitchLabel.style.display = "compact";
      }

      stateSwitch.addEventListener('click', function(){
        if (stateSwitch.checked){
          api.device.action(deviceId, "open", 'true').then((data) => {
            deviceMeta.state = this.checked;
            registerDeviceAction(deviceId, deviceMeta);
          });
        } else {
          api.device.action(deviceId, "close", 'true').then((data) => {
            deviceMeta.state = this.checked;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }

      });

      stateDropdown.addEventListener('change', function(){
        if(localStorage.getItem('routinesteps')){
          var action;
          if(stateDropdown.selectedIndex == 1){
            action = "open,true";
          }else if(stateDropdown.selectedIndex == 2){
            action = "close,true";
          }
          prepareRoutineStepPackage(action);
        }
      });

      lockSwitch.addEventListener('click', function(){
        if (lockSwitch.checked){
          api.device.action(deviceId, "lock", 'true').then((data) => {
            deviceMeta.locked = this.checked;
            registerDeviceAction(deviceId, deviceMeta);
          });
        } else {
          api.device.action(deviceId, "unlock", 'true').then((data) => {
            deviceMeta.locked = this.checked;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
      });

      lockDropdown.addEventListener('change', function(){
        console.log(lockDropdown.selectedIndex);
        if(localStorage.getItem('routinesteps')){
          var action;
          if(lockDropdown.selectedIndex == 1){
            action = "turnOn,true";
          }else if(lockDropdown.selectedIndex == 2){
            action = "turnOff,true";
          }
          prepareRoutineStepPackage(action);
        }
      });
      break;

    case ("device_blinds.html"):
      var switchLabel = document.getElementById('switchLabel');
      var dropdownLabel = document.getElementById('dropdownLabel');
      var stateSwitch = document.getElementById("stateSwitch");
      var stateDropdown = document.getElementById("stateDropdown");

      stateSwitch.checked = deviceMeta.pulled;

      if(localStorage.getItem('routinesteps')){
        dropdownLabel.style.display = "compact";
        switchLabel.style.display = "none";
        stateDropdown.selectedIndex = 0;
      }
      else{
        dropdownLabel.style.display = "none";
        switchLabel.style.display = "compact";
      }

      stateSwitch.addEventListener('click', function(){
        if (stateSwitch.checked)
          api.device.action(deviceId, "up", 'true').then((data) => {
            deviceMeta.pulled = stateSwitch.checked;
            registerDeviceAction(deviceId, deviceMeta);
          });
        else
          api.device.action(deviceId, "down", 'true').then((data) => {
            deviceMeta.pulled = stateSwitch.checked;
            registerDeviceAction(deviceId, deviceMeta);
          });
      });

      stateDropdown.addEventListener('change', function(){
        if(localStorage.getItem('routinesteps')){
          var action;
          if(stateDropdown.selectedIndex == 1){
            action = "down,true";
          }else if(stateDropdown.selectedIndex == 2){
            action = "up,true";
          }
          prepareRoutineStepPackage(action);
        }
      });
      break;

    case ("device_ac.html"):
      var textAC = document.getElementById('ac-temp');
      var stateSwitch = document.getElementById("stateSwitch");
      var temperatureSlider = document.getElementById("temperatureSlider");
      var modeSelect = document.getElementById("modeSelect");
      var verticalAngleSelect = document.getElementById("verticalAngleSelect");
      var horizontalAngleSelect = document.getElementById("horizontalAngleSelect");
      var fanSpeedSelect = document.getElementById("fanSpeedSelect");

      var switchLabel = document.getElementById('switchLabel');
      var dropdownLabel = document.getElementById('dropdownLabel');
      var textAC = document.getElementById('ac-temp');
      var stateSwitch = document.getElementById("stateSwitch");
      var stateDropdown = document.getElementById('stateDropdown');
      var temperatureSlider = document.getElementById("temperatureSlider");
      var modeSelect = document.getElementById("modeSelect");
      var verticalAngleSelect = document.getElementById("verticalAngleSelect");
      var horizontalAngleSelect = document.getElementById("horizontalAngleSelect");
      var fanSpeedSelect = document.getElementById("fanSpeedSelect");

      stateSwitch.checked = deviceMeta.state;
      temperatureSlider.value = deviceMeta.temperature;
      textAC.innerHTML = deviceMeta.temperature + "°C";
      modeSelect.value = deviceMeta.mode;
      verticalAngleSelect.value = deviceMeta.verticalAngle;
      horizontalAngleSelect.value = deviceMeta.horizontalAngle;
      fanSpeedSelect.value = deviceMeta.fanSpeed;

      if(localStorage.getItem('routinesteps')){
        switchLabel.style.display = "none";
        dropdownLabel.style.display = "compact";
        stateDropdown.selectedIndex = 0;
        modeSelect.selectedIndex = 0;
        verticalAngleSelect.selectedIndex = 0;
        horizontalAngleSelect.selectedIndex = 0;
        fanSpeedSelect.selectedIndex = 0;
      }
      else{
        dropdownLabel.style.display = "none";
        switchLabel.style.display = "compact";
      }

      stateSwitch.addEventListener('click', function(){
        if (stateSwitch.checked){
          api.device.action(deviceId, "turnOn", 'true').then((data) => {
            deviceMeta.state = stateSwitch.checked;
            registerDeviceAction(deviceId, deviceMeta);
          });
        } else{
          api.device.action(deviceId, "turnOff", 'true').then((data) => {
            deviceMeta.state = stateSwitch.checked;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }

      });

      stateDropdown.addEventListener('change', function(){
        if(localStorage.getItem('routinesteps')){
          var action;
          if(stateDropdown.selectedIndex == 1){
            action = "turnOn,true";
          }else if(stateDropdown.selectedIndex == 2){
            action = "turnOff,true";
          }
          prepareRoutineStepPackage(action);
        }
      });

      temperatureSlider.addEventListener('mouseup', function(){
          //si estas agregando pasos a la rutina:
          if(localStorage.getItem('routinesteps')){
            prepareRoutineStepPackage("setTemperature,"+ this.value.toString());
          }
          else{
            api.device.action(deviceId, "setTemperature", this.value).then((data) => {
              deviceMeta.temperature = this.value;
              registerDeviceAction(deviceId, deviceMeta);
            });
          }
          //console.log(this.id +" : " +this.value + "°C");
        });

      //solo para mostrar
      temperatureSlider.addEventListener('input', function(){
        textAC.innerHTML = this.value +"°C";
      });

      modeSelect.addEventListener('change', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setMode,"+ this.value.toString());
        }
        else{
          api.device.action(deviceId, "setMode", this.value).then((data) => {
            deviceMeta.mode = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log("Este selector ahora es: " + this.value);
      });

      verticalAngleSelect.addEventListener('change', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setVerticalSwing,"+ this.value.toString());
        }
        else{
          api.device.action(deviceId, "setVerticalSwing", this.value).then((data) => {
            deviceMeta.verticalAngle = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log("Este selector ahora es: " + this.value);
      });

      horizontalAngleSelect.addEventListener('change', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setHorizontalSwing,"+ this.value.toString());
        }
        else{
          api.device.action(deviceId, "setHorizontalSwing", this.value).then((data) => {
            deviceMeta.horizontalAngle = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log("Este selector ahora es: " + this.value);
      });

      fanSpeedSelect.addEventListener('change', function(){
        //si estas agregando pasos a la rutina:
        if(localStorage.getItem('routinesteps')){
          prepareRoutineStepPackage("setFanSpeed,"+ this.value.toString());
        }
        else{
          api.device.action(deviceId, "setFanSpeed", this.value).then((data) => {
            deviceMeta.fanSpeed = this.value;
            registerDeviceAction(deviceId, deviceMeta);
          });
        }
        //console.log("Este selector ahora es: " + this.value);
      });
      break;
  }

}

function registerDeviceAction(deviceId, deviceMeta){
  var deviceObject = {
      "typeId": deviceId,
      "name": deviceName,
      "meta": JSON.stringify(deviceMeta)
  };
  console.log("Device: " + JSON.stringify(deviceObject));

  api.device.modify(deviceId, JSON.stringify(deviceObject));
}

function prepareRoutineStepPackage(text){
  //desactivar el modo
  localStorage.removeItem('routinesteps');
  //agregar la habitación, device y acción;
  var action = roomName+","+deviceName+","+deviceId+",";
  action+=text;
  //guardar termporalmente para pasarle al objeto currentRoutine
  localStorage.setItem('actionList', action);
  //indicamos que terminamos de agregar un paso
  localStorage.setItem('finishAdding', true);
  window.location = localStorage.getItem('routinePage');
}

function isInvalidString(string){
  for(var i=0; i<string.length; i++)
  {
    if(!((string[i]>='a' && string[i]<='z') || (string[i]>='A' && string[i]<='Z') || string[i]==' ' || (string[i]>='0' && string[i]<='9')))
      return true;
  }
  return false;
}

function errorMsg(msg){
  errorcontainer.style.display = "block";
  var tmp = errorMsgHTML.replace(/{errorMsg}/, msg);
  errorcontainer.innerHTML = tmp;
  document.getElementById("modalError").setAttribute("class", "alert alert-danger");
}
