var addRoomElementHTML = '\
<li id="createBtn"><div class="row room-label bg-secondary">\
<button id="addRoomBtn" class="btn btn-secondary device-label col">\
<p class="add-device-label-text">Crear nueva habitación</p>\
</button></div></li>\
';

var roomElementHTML = '\
<li>\
  <div class="row room-label bg-primary">\
    <a class="btn btn-primary device-label col" href="{roomLink}">\
      <p class="device-label-text">{roomName}</p>\
    </a>\
  </div>\
</li>';

var errorMsgHTML = '\
<div id="modalError" class="alert alert-danger">\
<h0 id="modalErrorText">{errorMsg}</h0>\
</div>';

var roomNameList = [];
var errorcontainer;
var regNavBar;
var routineNavBar;

window.addEventListener('load', function()
{
	var roomListElement = document.getElementById('roomsList');
  errorcontainer = document.getElementById('errors');
  errorcontainer.style.display = "none";
  regNavBar = document.getElementById('regular-tabs');
  routineNavBar = document.getElementById('in-routine-tabs');
  if(localStorage.getItem('routinesteps'))
  {
    regNavBar.style.display = "none";
    routineNavBar.style.display = "block";
  }
  else{
    regNavBar.style.display = "block";
    routineNavBar.style.display = "none";
  }

	api.room.getAll()
      .then((data) => {
        roomsList = data;
        if(localStorage.getItem('routinesteps'))
        {
          roomListElement.innerHTML = "";
        }
        else{
          roomListElement.innerHTML = addRoomElementHTML;
        }
        if (roomsList.rooms != undefined){
          addRoomNamesToList(data.rooms);
          roomListElement.innerHTML += generateHTMLFromRoomsList(roomsList);
        }
        if(!(localStorage.getItem('routinesteps')))
        {
          setupAddRoomPopup();
        }
        else{
          var switches = document.getElementsByClassName('room-label-switch');
          for(var i=0; i<switches.length; i++)
          {
            switches[i].style.display = "none";
          }
        }
      })
      .catch((error) => {
        roomListElement.innerHTML = 'Request failed: ' + error;
      });
});

function addRoomNamesToList(roomList){
  for (i = 0; i < roomList.length; i++){
    roomNameList.push(roomList[i].name);
  }
}

function generateHTMLFromRoomsList(list){
  var result = '';
  var i;
  for (i = 0; i < list.rooms.length; i++){
    var temp = roomElementHTML.replace(/{roomLink}/, "room_view.html#" +list.rooms[i].id);
    result += temp.replace(/{roomName}/, list.rooms[i].name);
  }
  return result;
}

function setupAddRoomPopup(){
  var addRoomBtn = document.getElementById("addRoomBtn");

  var modal = document.getElementById('myModal');
  var closeBtn = document.getElementsByClassName("close")[0];
  var createBtn = document.getElementById("create-button");

  var roomNameInputElem = document.getElementById("roomNameInput");
  roomNameInputElem.value = '';

  addRoomBtn.onclick = function() {
    modal.style.display = "block";
  }

  closeBtn.onclick = function() {
    modal.style.display = "none";
  }

  createBtn.onclick = function(){
    if (roomNameInputElem.value == ""){
      errorMsg("Por favor, introduzca un nombre para la habitación.");
      return;
    }
    else if(roomNameInputElem.value.length < 3){
      errorMsg("Por favor, introduzca un nombre con al menos 3 caracteres.");
      return;
    }
    else if (roomNameList.includes(roomNameInputElem.value)){
      errorMsg("Ya existe una habitación con ese nombre. Por favor, escoja otro.");
      return;
    }
    else if(isInvalidString(roomNameInputElem.value))
    {
      errorMsg("El nombre ingresado contiene caracteres inválidos. Por favor, sólo utilice letras, números y espacios. No se admiten tildes o la letra Ñ.", errorcontainer);
      return;
    }

    createBtn.style = "background:grey;";
    var newRoom = {
      "name": roomNameInputElem.value,
      "meta": "{}"
    };

    api.room.add(newRoom)
      .then((data) => {
        window.location.reload(false);
      })
      .catch((error) => {
        document.write(error + " - While creating room");//TODO: ERROR HANDLING
      });
  }

  // cuando aprieta fuera de pop-up se cierra
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
      errorcontainer.style.display = "none";
    }
  }
}

function errorMsg(msg){
  document.getElementById("modalError").className = "alert alert-danger";
  errorcontainer.style.display = "block";
  var tmp = errorMsgHTML.replace(/{errorMsg}/, msg);
  errorcontainer.innerHTML = tmp;
}

function isInvalidString(string){
  for(var i=0; i<string.length; i++)
  {
    if(!((string[i]>='a' && string[i]<='z') || (string[i]>='A' && string[i]<='Z') || string[i]==' ' || (string[i]>='0' && string[i]<='9')))
      return true;
  }
  return false;
}
