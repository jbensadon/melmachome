var deviceTypeLinkMap = new Map();
deviceTypeLinkMap.set("im77xxyulpegfmv8", "device_oven.html");
deviceTypeLinkMap.set("rnizejqr2di0okho", "device_fridge.html");
deviceTypeLinkMap.set("go46xmbqeomjrsjr", "device_lights.html");
deviceTypeLinkMap.set("lsf78ly0eqrjbz91", "device_door.html");
deviceTypeLinkMap.set("eu0v2xgprrhhg41g", "device_blinds.html");
deviceTypeLinkMap.set("li6cbv5sdlatti0j", "device_ac.html");

var errorMsgHTML = '\
<div id="modalError" class="alert alert-danger">\
<h0 id="modalErrorText">{errorMsg}</h0>\
</div>';

var addDeviceElementHTML = '\
<li id="createBtn">\
      <div class="row screen-label">\
      <button id="addDeviceBtn" class="btn btn-secondary device-label col">\
      <p class="add-device-label-text">Agregar nuevo dispositivo</p>\
      </button>\
      </div>\
    </li>';

var addSwitchlessElementHTML = '\
<li>\
      <div class="row screen-label bg-light">\
        <a class="btn btn-light device-label col" href="{deviceLink}">\
          <p class="device-label-text">{deviceName}</p>\
        </a>\
      </div>\
    </li>';

// para la idea original, ofrecer switches a los dispositivos que puedan encenderse por un switch
var addSwitchfulElementHTML = '\
    <li>\
      <div class="row screen-label bg-light">\
        <a class="btn btn-light device-label col" href="{deviceLink}">\
          <p class="device-label-text">{deviceName}</p>\
        </a>\
        <div>\
          <label class="switch">\
            <input class="switch-input" type="checkbox">\
            <span class="switch-slider round"></span>\
          </label>\
        </div>\
      </div>\
    </li>';

var deviceNameList = [];
var roomNameList = [];
var roomId = window.location.href.split('#')[1];
var roomTitle = document.getElementsByClassName("screen-title")[0];
var regNavBar;
var routineNavBar;

window.addEventListener('load', function()
{
  var deviceListElement = document.getElementById('deviceList');
  regNavBar = document.getElementById('regular-tabs');
  routineNavBar = document.getElementById('in-routine-tabs');
  if(localStorage.getItem('routinesteps'))
  {
    regNavBar.style.display = "none";
    routineNavBar.style.display = "block";
  }
  else{
    regNavBar.style.display = "block";
    routineNavBar.style.display = "none";
  }
  api.room.getDevices(roomId).then((data) => {
    devicesList = data;
    if(localStorage.getItem('routinesteps'))
    {
      deviceListElement.innerHTML = "";
    }
    else{
      deviceListElement.innerHTML = addDeviceElementHTML;
    }
    if (devicesList.devices != undefined){
      deviceListElement.innerHTML += generateDeviceHTMLFromDevicesList(devicesList);
    }
    if(!(localStorage.getItem('routinesteps')))
    {
      setupAddDevicePopup();
      setupEditRoomPopup();
      setupDeleteRoomPopup();
    }
    else {
      var editBtn = document.getElementById('edit-button');
      var trashBtn = document.getElementById('trash-button');
      var switches = document.getElementsByClassName('switch');
      editBtn.style.display = "none";
      trashBtn.style.display = "none";
      for(var i=0; i<switches.length; i++)
      {
        switches[i].style.display = "none";
      }
    }
  });
  api.room.getAll().then((data) =>{
    roomsList = data;
    if(roomsList.rooms != undefined)
    {
      addRoomNamesToList(data.rooms);
    }
  });
  api.device.getAll().then((data) =>{
    deviceList = data;
    if(deviceList.devices != undefined)
    {
      addDeviceNamesToList(data.devices);
    }
  });

  var roomName;
  api.room.get(roomId).then((data) => {
    if(data.room != undefined)
    {
      roomName = data.room.name;
      roomTitle.innerHTML = roomName;
    }
  });
});

function addDeviceNamesToList(deviceList){
  for (i = 0; i < deviceList.length; i++){
    deviceNameList.push(deviceList[i].name);
  }
}

function addRoomNamesToList(roomList){
  for (i = 0; i < roomList.length; i++){
    roomNameList.push(roomList[i].name);
  }
}

function generateDeviceHTMLFromDevicesList(list){
  var result = '';
  var i, temp;
  for (i = 0; i < list.devices.length; i++)
  {
    temp = addSwitchlessElementHTML.replace(/{deviceLink}/, deviceTypeLinkMap.get(list.devices[i].typeId) + "#" + list.devices[i].id +"#" +roomId);
    result += temp.replace(/{deviceName}/, list.devices[i].name);
  }
  return result;
}

function setupAddDevicePopup(){
  var addDeviceBtn = document.getElementById("addDeviceBtn");
  var errorcontainer = document.getElementById('addErrors');

  var modal = document.getElementById('myModal');
  var closeBtn = document.getElementById("add-close-button");
  var createBtn = document.getElementById("create-button");

  var deviceNameInputElem = document.getElementById("deviceNameInput");
  var deviceTypeSelectElem = document.getElementById("deviceTypeInput");
  deviceNameInputElem.value = '';

  addDeviceBtn.onclick = function() {
    modal.style.display = "block";
  }

  closeBtn.onclick = function() {
    modal.style.display = "none";
  }


  createBtn.onclick = function(){
    if (deviceNameInputElem.value == ""){
      errorMsg("Por favor, introduzca un nombre para el dispositivo.", errorcontainer);
      return;
    }
    else if(deviceNameInputElem.value.length < 3){
      errorMsg("Por favor, introduzca un nombre con al menos 3 caracteres.", errorcontainer);
      return;
    }
    else if (deviceNameList.includes(deviceNameInputElem.value)){
      errorMsg("Ya existe un dispositivo con ese nombre. Por favor, escoja otro.", errorcontainer);
      return;
    }
    else if(isInvalidString(deviceNameInputElem.value))
    {
      errorMsg("El nombre ingresado contiene caracteres inválidos. Por favor, sólo utilice letras, números y espacios. No se admiten tildes o la letra Ñ.", errorcontainer);
      return;
    }
    createBtn.style = "background:grey;";

    switch(deviceTypeSelectElem.options[deviceTypeSelectElem.selectedIndex].value){
      case "im77xxyulpegfmv8": //Oven
        var deviceMeta = oven_meta;
        break;
      case "rnizejqr2di0okho": //Fridge
        var deviceMeta = fridge_meta;
        break;
      case "go46xmbqeomjrsjr": //Lights
        var deviceMeta = lamp_meta;
        break;
      case "lsf78ly0eqrjbz91": //Door
        var deviceMeta = door_meta;
        break;
      case "eu0v2xgprrhhg41g": //Blinds
        var deviceMeta = blinds_meta;
        break;
      case "li6cbv5sdlatti0j": //Air Conditioner
        var deviceMeta = ac_meta;
        break;
    }
    deviceMeta.room = roomId;

    var newDevice = {
      "typeId": deviceTypeSelectElem.options[deviceTypeSelectElem.selectedIndex].value,
      "name": deviceNameInputElem.value,
      "meta": JSON.stringify(deviceMeta)
    };

    api.device.add(newDevice)
      .then((data) => {
        api.device.setDeviceRoom(data.device.id, roomId)
          .then((data) => {
            window.location.reload(false);
          })
          .catch((error) => {
            document.write(error + " - While adding device to room.");//TODO: ERROR HANDLING
          });

        })
        .catch((error) => {
          document.write(error + " - While creating device");//TODO: ERROR HANDLING
        });
    }

  // cuando aprieta fuera de pop-up se cierra
  window.onclick = function(event) {
    if (event.target == modal)
      modal.style.display = "none";
  }
}

function setupEditRoomPopup()
{
  var editModal = document.getElementById('editModal');
  var editBtn = document.getElementById('edit-button');
  var closeBtn = document.getElementById('edit-close-button');
  var saveBtn = document.getElementById('rename-button');
  var roomNameInputElem = document.getElementById("roomNameInput");
  var errorcontainer = document.getElementById('editErrors');

  api.room.get(roomId).then((data) => {
    if(data.room != undefined)
      roomNameInputElem.value = data.room.name;
  });

  editBtn.onclick = function(){
    editModal.style.display = "block";
  }

  closeBtn.onclick = function(){
    editModal.style.display = "none";
  }

  saveBtn.onclick = function(){
    if (roomNameInputElem.value == ""){
      errorMsg("Por favor, introduzca un nombre para la habitación.", errorcontainer);
      return;
    }
    else if(roomNameInputElem.value.length < 3){
      errorMsg("Por favor, introduzca un nombre con al menos 3 caracteres.", errorcontainer);
      return;
    }
    else if ((roomNameInputElem.value != roomTitle.innerHTML && roomNameList.includes(roomNameInputElem.value)))
    {
      errorMsg("Ya existe una habitación con ese nombre. Por favor, escoja otro.", errorcontainer);
      return;
    }
    else if(isInvalidString(roomNameInputElem.value))
    {
      errorMsg("El nombre ingresado contiene caracteres inválidos. Por favor, sólo utilice letras, números y espacios. No se admiten tildes o la letra Ñ.", errorcontainer);
      return;
    }
    saveBtn.style = "background:grey;";
    api.room.get(roomId).then((data) =>{
      var newRoom = {
        "name": roomNameInputElem.value,
        "meta": "{}"
      };
      api.room.modify(roomId, newRoom).then((data) =>{
        window.location.reload();
      });
    }).catch((error) => {
          document.write(error + " - While renaming room");//TODO: ERROR HANDLING;
  });
  }

  window.addEventListener('click', function(event){
    if (event.target == editModal)
      editModal.style.display = "none";
  });
}

function setupDeleteRoomPopup()
{
  var trashBtn = document.getElementById('trash-button');
  var cancelBtn = document.getElementById('cancel-button');
  var deleteBtn = document.getElementById('delete-button');
  var delModal = document.getElementById('deleteModal');

  trashBtn.onclick = function() {
    console.log("HOLA");
    delModal.style.display = "block";
  }

  cancelBtn.onclick = function() {
    delModal.style.display = "none";
  }

  deleteBtn.onclick = function() {
    api.room.getDevices(roomId).then((data) => {
      if(data.devices != undefined)
      {
        for(var i=0; i<data.devices.length; i++)
        {
          api.device.delete(data.devices[i].id).then((data)=>{
          });
        }
        api.room.delete(roomId).then((data) => {
          window.location.href = "rooms.html";
        });
      }
    });
  }

  window.addEventListener('click', function(event){
    if (event.target == delModal)
      delModal.style.display = "none";
  });
}

function errorMsg(msg, errorcontainer){
  errorcontainer.style.display = "block";
  var tmp = errorMsgHTML.replace(/{errorMsg}/, msg);
  errorcontainer.innerHTML = tmp;
  document.getElementById("modalError").setAttribute("class", "alert alert-danger");
}

function isInvalidString(string){
  for(var i=0; i<string.length; i++)
  {
    if(!((string[i]>='a' && string[i]<='z') || (string[i]>='A' && string[i]<='Z') || string[i]==' ' || (string[i]>='0' && string[i]<='9')))
      return true;
  }
  return false;
}
