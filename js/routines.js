var addRoutineElementHTML = '\
<li id="createBtn"><a type="button" id="addRoutineBtn" class="btn btn-secondary btn-lg btn-block" href="#">Crear nueva rutina</a></li>\
';


var routineElementHTML = '\
<li>\
  <div class="btn-group" role="group" style="width:100%;">\
    <a type="button" class="btn btn-primary btn-lg btn-block" href="routines_view.html#{routineID}">{routineName}</a>\
    <a type="button" class="btn btn-dark" onclick=executeRoutine(this)><span class="fa fa-play" style="color: #ffffff;"></span></a>\
  </div>\
</li>';


var errorMsgHTML = '\
<div id="modalError" class="alert alert-danger">\
<h0 id="modalErrorText">{errorMsg}</h0>\
</div>';

var routineNameList = [];
var errorcontainer;
var devicesList;

window.addEventListener('load', function()
{

  localStorage.clear();
  errorcontainer = document.getElementById('errors');
  errorcontainer.style.display = "none";

  api.device.getAll()
    .then((data) =>{
      devicesList = data;
    })

    .catch((error) => {
      routineListElement.innerHTML = 'Request failed: ' + error;
    });

  var routinesList;
	var routineListElement = document.getElementById('routinesList');
	api.routine.getAll()
      .then((data) => {
        routinesList = data;
        routineListElement.innerHTML = addRoutineElementHTML;
        if (routinesList.routines != undefined){
          addRoutineNamesToList(data.routines);
          routineListElement.innerHTML += generateHTMLFromRoutinesList(routinesList);
        }
        setupAddRoutinePopup();

      if(devicesList.devices.length == 0){
          errorMsg("Por favor, primero agregue un dispositivo.");
          document.getElementById("modalError").setAttribute("class", "alert alert-warning");
          return;
        }
        replaceRoutineLinks(routinesList);
      })
      .catch((error) => {
        routineListElement.innerHTML = 'Request failed: ' + error;
      });


});

function addRoutineNamesToList(routineList){
  for (i = 0; i < routineList.length; i++){
    routineNameList.push(routineList[i].name);
  }
}

function generateHTMLFromRoutinesList(list){
  var result = '';
  var i;
  for (i = 0; i < list.routines.length; i++){
    //var temp = routineElementHTML.replace(/{imgSrc}/, deviceTypeIconMap.get(list.devices[i].typeId));
    result += routineElementHTML.replace(/{routineName}/g, list.routines[i].name);
  }
  return result;
}

function setupAddRoutinePopup(){
  var addRoutineBtn = document.getElementById("addRoutineBtn");

  var modal = document.getElementById('myModal');

  var closeBtn = document.getElementsByClassName("close")[0]; //hardcodeado

  var createBtn = document.getElementById("create-button");

  var routineNameInputElem = document.getElementById("routineNameInput");
  routineNameInputElem.value = '';

  addRoutineBtn.onclick = function() {
    modal.style.display = "block";
  }

  closeBtn.onclick = function() {
    modal.style.display = "none";
  }

  createBtn.onclick = function(){
    if (routineNameInputElem.value == ""){
      errorMsg("Por favor, introduzca un nombre para la rutina");
      return;
    }
    else if(routineNameInputElem.value.length < 3){
      errorMsg("Por favor, introduzca un nombre con al menos 3 caracteres.");
      return;
    }
    else if(devicesList.devices.length == 0)
    {
      errorMsg("Por favor, primero agregue un dispositivo.");
      return;
    }
    else if (routineNameList.includes(routineNameInputElem.value)){
      errorMsg("Ya existe una rutina con ese nombre. Por favor, escoja otro.");
      return;
    }
    else if(isInvalidString(routineNameInputElem.value))
    {
      errorMsg("El nombre ingresado contiene caracteres inválidos. Por favor, sólo utilice letras, números y espacios. No se admiten tildes o la letra Ñ.");
      return;
    }

    createBtn.style = "background:grey;";
    window.location.href = `routines_view.html#${routineNameInputElem.value}`;
  }

  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
      errorcontainer.style.display = "none";
    }
  }
}

function errorMsg(msg){
  errorcontainer.style.display = "block";
  var tmp = errorMsgHTML.replace(/{errorMsg}/, msg);
  errorcontainer.innerHTML = tmp;
  document.getElementById("modalError").setAttribute("class", "alert alert-danger");
}

function replaceRoutineLinks(routinesList){
  var tags = document.getElementsByTagName("a");
  for(var i = 0; i < routinesList.routines.length; i++){
    for(var j = 0; j < tags.length; j++){
      if(tags[j].textContent == routinesList.routines[i].name){
        tags[j].setAttribute("href", "routines_view.html#"+
                                    routinesList.routines[i].name+"#"+
                                    routinesList.routines[i].id);
      }

    }
  }
}

function isInvalidString(string){
  for(var i=0; i<string.length; i++)
  {
    if(!((string[i]>='a' && string[i]<='z') || (string[i]>='A' && string[i]<='Z') || string[i]==' ' || (string[i]>='0' && string[i]<='9')))
      return true;
  }
  return false;
}

function executeRoutine(obj){
  var id = obj.parentElement.getElementsByTagName('a')[0].href.split('#')[2];
  api.routine.execute(id);
  console.log("done");
}
