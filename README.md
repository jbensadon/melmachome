# MelmacHome

Para ejecutar:

Primero, tener la API corriendo corriendo el comando npm start en la carpeta
de la api.

Segundo, descargar el repo e ir a la carpeta "melmachome".
Desde allí, si estamos en linux, abrimos una terminal.

Tercero, correr el comando "http-server ./ ". Este comando va a imprimir la
dirección en donde esta hospedada la aplicación. Por ejemplo, al ejecutar
el comando obtengo:

Starting up http-server, serving ./D
Available on:
  http://127.0.0.1:8081
  http://192.168.0.12:8081
Hit CTRL-C to stop the server

Cuarto paso, abrimos un navegador y escribimos: "localhost:8081"

Navegadores soportados:
-Google Chrome
-Firefox
-Waterfox
-Vivaldi
